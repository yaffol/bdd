Feature: Managing Users

  Every organisation has users. These are the email address and password
  pairs that allow people to login to the Crossref Frontend to view and make
  changes to organisations for which they have permissions.

  Scenario: Support Team adds a user to an organisation
  
   Given Josh, a Support Team member
     And Bill, an employee of Psychoceramics Inc.
    When Josh adds Bill as a user to Psychoceramics Inc.
    Then Bill receives a welcome email with a link to activate his account

  Scenario: New user enables their account

   Given Bill, an employee of Psychoceramics Inc.
     And an welcome email sent less than 24 hours ago
    When Bill clicks the activation link in his welcome email
    Then Bill can login to his account

  Scenario: Account activation link only works once

   Given Bill, an employee of Psychoceramics Inc.
     And an active acount for bill@psychocermics.org
    When Bill clicks the activation link in his welcome email
    Then he receives a message that his account has already been activated