Feature: Managing Organisations

  Organisations represent the bodies that Crossref interacts with.
  In order to be available within the API Key Manager, the Support
  Team needs to add the Organisation. In this process, they will
  copy across the Organisation Name and ID from the Sugar CRM.

  Scenario Outline: Adding an organisation and setting Plus access

    Given Josh, a Support Team member
     When he adds <organisation>
      And sets their Plus access to <enabled?>
     Then <organisation> <has?> Plus access


    | organisation        | enabled?    | has?          |
    | Psychocermics       | enabled     | has           |
    | Wesleyan University | not enabled | does not have |

  Scenario: Adding an organisation and enabling Plus access

    Given Josh, a Support Team member
     When he adds Psychocermics
      And sets their Plus access to enabled
     Then Psychocermics has Plus access

  Scenario: Adding an organisation and not enabling Plus access

    Given Josh, a Support Team member
     When he adds Wesleyan University
      And sets their Plus access to not enabled
     Then Wesleyan University does not have Plus access

  Scenario: Adding an organisation with the same name as an existing organisation

    Given Josh, a Support Team member
      And an Organisation called "Psychoceramics"
     When Josh adds a new organisation called "Psychoceramics"
      And he ignores the warning
     Then the new organisation also called "Psychoceramics" should be added

    Scenario: Preventing creation of new organisation with the same ID as an existing organisation

    Given Josh, a Support Team member
      And an Organisation with the ID "12345"
     When Josh adds a new organisation with the ID "12345"
     Then he cannot create the new organisation
      And is shown a message explaining why


