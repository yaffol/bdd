Feature: Features which belong in the API specifications, moved from the UI specs.

  Scenario Outline: Searching for organisations and filtering on Metadata Plus access

    An organisation's access to the  Metadata Plus service can be enabled or disabled.
    Searching for organisations when also specifying a Metadata Plus service filter
    works the same was as above, except that the organisations' Metadata Plus service status
    is taken into account.

    Given Josh, a Support Team member
     When he searches for <query> and <filter>
     Then he should see <results>

    Examples:

    | query         | filter                      | results                                      |
    | psychocermics | has metadata plus           | "Psyschoceramics"                            |
    | psychocermics | does not have metadata plus | no results                                   |
    | empty query   | does not have metadata plus | "Dept. of Psychology", "Wesleyan University" |
    