Feature: Controling access to Metadata Plus

  Metadata Plus is the premium access service to Crossref Metadata.
  Subscribers pay to gain access to the private "Plus" API server pool.
  Organisations login to create new, or manage their existing API keys.

  Scenario: enable Metadata Plus for an organisation

   Given Josh, a Support Team member
     And Jemina, an administrator for Psychoceramics Inc.
    When Josh enables Metadata Plus services for Psychoceramics Inc.
    Then Jemima should be able to create and manage API keys for Psychoceramics Inc.

  Scenario: disable Metadata Plus for an organisation

   Given Josh, a Support Team member,
     And Jemima, an administrator for Psychoceramics Inc.
     And Sarah, a developer for Psychoceramics Inc. with an API key
    When Josh disables Metadata Plus access for Psychoceramics Inc.
    Then Jemima should not be able to login
     And she should not be able to create or manage API keys for Psychoceramics Inc.
     And Sarah should not be able to make API queries using her API key

  Scenario: Disabling Metadata Plus for an organisation prevents login

   Given Josh, a Support Team member,
     And Jemima, an administrator for Psychoceramics Inc.
    When Josh disables Metadata Plus access for Psychoceramics Inc.
    Then Jemima should not be able to login
     And she should not be able to create or manage API keys for Psychoceramics Inc.

  Scenario: Disabling Metadata Plus for an organisation disables API keys

   Given Josh, a Support Team member,
     And Sarah, a developer for Psychoceramics Inc. with an API key
    When Josh disables Metadata Plus access for Psychoceramics Inc.
     And Sarah should not be able to make API queries using her API key

     