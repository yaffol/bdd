Feature: Managing keys

  Organisations require API keys are the tokens which allow Organisations 
  to access the private server pool of the Metadata Plus service.
  
  Scenario: Disabling a key prevents Plus service API access

    Given Sarah, a developer for Psychoceramics Inc.
      And an API key related to her user account
      And Josh, a Support Team member
     When Josh disables Sarah's API key
     Then Sarah should not be able to make API requests using her key
      And she should receive an appropriate HTTP code and explanatory message

      
