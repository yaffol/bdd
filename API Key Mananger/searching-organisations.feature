Feature: Searching Organisations

  Crossref deals with Organisations - these could be members,
  subscribers to the Plus service, or neither.
  Every User of the API Key management system will belong to an
  organisation. In order to be able to work with the API keys 
  or users of an organisation, the Support Team needs to be able
  to find them via a drop-down menu.

  TODO: currently the intent is for the UI to have a single search box
  whereas the API has individual search parameters - this disparity
  needs to be resolved.

  Background:
    Given an Organisation called "Psychocermics" with ID "12345" and Metadata Plus service "enabled"
      And an Organisation called "Dept. of Psychology" with ID "67890" and Metadata Plus service "disabled"
      And an Organisation called "Wesleyan University" with ID "19283" and Metadata Plus service "disabled"

  Scenario Outline: Searching for organisations

    Every organisation has a name and and ID. Matching is done on
    either - a name can be a partial match, but an ID can only be a 
    complete match. All matches are case-insensitive.

    Given Josh, a Support Team member
     When he searches for <query>
     Then he should see <results> in a drop-down menu

    Examples: 

    | query              | results                                 |
    | psychocermics      | "Psychoceramics"                        |
    | psycho             | "Psychoceramics", "Dept. of Psychology" |
    | PSYchO             | "Psychoceramics", "Dept. of Psychology" |
    | query              | results                                 |
    | psychoceramics     | "Psychoceramics"                        |
    | psycho             | "Psychoceramics", "Dept. of Psychology" |
    | PSYchO             | "Psychoceramics", "Dept. of Psychology" |
    | Oxford             | no results                              |
    | 12345              | "Psychoceramics"                        |
    | 1234               | no results                              |
    | 6789               | no results                              |
    | 67890              | "Dept. of Psychology"                   |
    | 12345 Dept         | "Dept. of Psychology", "Psychoceramics" |
    | Dept of Psychology | no results                              |

  