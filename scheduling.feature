Feature: Scheduling

  Because Scheduling is a huge functionality, this
  specification file describes on the most important
  high-level scenario.
  You can find more detailed scenarios in the rest
  of the files inside the "meetings" folder in the
  specification suite.

  In order to have meetings together
  without booking conflicting times, team members
  need to be able to see each others existing meetings.
  In order that confidential information is kept private,
  members of different teams cannot see each others
  meetings.

  Scenario: Creating a meeting successfully

    Given Mike, a member of our team
      And that it isn't 2 p.m. yet
     When Mike chooses 2 p.m. as a start time for his meeting
     Then he should be able to save his meeting


  Scenario: Failing at creating a new meeting

    Given Mike, a member of our team
      And that it's already 2 p.m.
     When Mike chooses 2 p.m. as a start time for his meeting
     Then he shouldn't be able to save his meeting

  Scenario: Failing at creating a new meeting with a scheduling conflict

    Given Mike, a member of our team
      And another event in Mike's calendar at 2 p.m.
     When Mike chooses 2 p.m. as a start time for his meeting
     Then he should not be able to save his meeting    

  Scenario: Cancelling a meeting
    Given a meeting by John and Anna at 4 p.m.
      And that it is 3 p.m. now
     When Anna cancels the meeting
     Then the event should be removed from the calendar
      And John should be notified about the canceled event



  