Feature: Outputs

  Scenario: Exporting documents

  Given a draft to export
   When Vladimir saves "Lola" as PDF
   Then a formatted PDF should appear where Vladimir saved it

  Scenario: Printing documents

  Given a 50 pages long draft
   When Vladimir wants to print the entire draft as low-quality A4
   Then the printer should print 50 low quality A4 pages

  Scenario: A new book

    Given a new book idea by Vladimir

  Scenario: Improving readability score
    Given Vladimir's low readability score
     When he shortens a sentence
     Then his readability score should improve

    